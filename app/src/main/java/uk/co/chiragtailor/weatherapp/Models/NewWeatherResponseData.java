package uk.co.chiragtailor.weatherapp.Models;

/**
 * Created by Naresh on 30-05-2017.
 */

public class NewWeatherResponseData extends BaseDataResponse {

    public String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
