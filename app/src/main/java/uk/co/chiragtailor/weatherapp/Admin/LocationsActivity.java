package uk.co.chiragtailor.weatherapp.Admin;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.co.chiragtailor.weatherapp.Activity.BaseActivity;
import uk.co.chiragtailor.weatherapp.Adapter.LocationsAdapter;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Validation;
import uk.co.chiragtailor.weatherapp.HelperLibrary.WeatherConfig;
import uk.co.chiragtailor.weatherapp.Models.AddLocationResponseData;
import uk.co.chiragtailor.weatherapp.Models.LocationsDeleteResponseData;
import uk.co.chiragtailor.weatherapp.Models.LocationsResponse;
import uk.co.chiragtailor.weatherapp.Models.LocationsResponseData;
import uk.co.chiragtailor.weatherapp.Models.LoginModel;
import uk.co.chiragtailor.weatherapp.R;

public class LocationsActivity extends BaseActivity {

    //Widgets-------------
    RecyclerView rvLocations;
    public Toolbar toolbar;
    public TextView tvTitle;
    //--------------------

    //Variables-----------
    LocationsAdapter adapterLocations;
    Activity activity;
    ArrayList<LocationsResponse> alLocatiosns = new ArrayList<>();
    //--------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);

        findView();
        setListeners();
        init();

        if (Utils.isNetworkAvailable(activity, false, false)) {
            Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
            getLocationsList();
        }
    }

    @Override
    public void findView() {
        rvLocations = (RecyclerView) findViewById(R.id.rvLocations);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvTitle);
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void init() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvTitle.setText(getString(R.string.locations));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        activity = LocationsActivity.this;
        rvLocations.setLayoutManager(new LinearLayoutManager(rvLocations.getContext()));
        rvLocations.setItemAnimator(new DefaultItemAnimator());
        rvLocations.setHasFixedSize(true);

        adapterLocations = new LocationsAdapter(activity, alLocatiosns, new LocationsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

                switch (view.getId()) {

                    case R.id.ivLocationsDelete:

                        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyAlertDialog);
                        builder.setMessage("Are you sure to want to delete location?");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                deleteLocation(alLocatiosns.get(position).get_id(), position);
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                        break;

                    case R.id.llLocations:
                        startActivity(new Intent(LocationsActivity.this, SelectDateActivity.class).putExtra("locationID", alLocatiosns.get(position).get_id()));
                        break;
                }

            }
        });
        rvLocations.setAdapter(adapterLocations);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_locations, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addLocation:
                new MaterialDialog.Builder(this)
                        .title("Add Location")
                        //.content(R.string.input_content)
                        .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT)
                        .input(R.string.add_location_hint, R.string.blank, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                if (input.length() > 0) {
                                    if (Utils.isNetworkAvailable(activity, false, false)) {
                                        Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
                                        addLocations(input);
                                    } else {
                                        showToastMessage(getResources().getString(R.string.network_alert));
                                    }
                                }

                            }
                        }).show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addLocations(final CharSequence input) {
        try {
            String url = WeatherConfig.url + WeatherConfig.urlLocations;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Utils.hideProgressDialog();
                    Gson gson = new Gson();
                    AddLocationResponseData addLocationResponseData = gson.fromJson(response, AddLocationResponseData.class);

                    switch (addLocationResponseData.getSuccess()) {
                        case "1": // response is success
                            showToastMessage(addLocationResponseData.getMessage());

                            if (Utils.isNetworkAvailable(activity, false, false)) {
                                Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
                                getLocationsList();
                            }

                            break;
                        case "0":
                            showToastMessage(addLocationResponseData.getMessage());
                        default:
                            break;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", input.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteLocation(String id, final int position) {

        try {
            Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));

            String url = WeatherConfig.url + WeatherConfig.urlLocations + "/" + id;

            StringRequest stringRequest = new StringRequest(StringRequest.Method.DELETE, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    Utils.hideProgressDialog();

                    Gson gson = new Gson();
                    LocationsDeleteResponseData locationsDeleteResponseData = gson.fromJson(response, LocationsDeleteResponseData.class);

                    switch (locationsDeleteResponseData.getSuccess()) {

                        case "1": // response is success
                            alLocatiosns.remove(position);
                            adapterLocations.notifyDataSetChanged();
                            break;
                        case "0":

                        default:
                            break;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    Toast.makeText(LocationsActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                /*@Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                        params.put("email", emailText);
                    params.put("password", passwordText);
                    return params;
                }*/
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } catch (Exception e) {

        }

    }

    public void showToastMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private void getLocationsList() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlLocations;

            StringRequest stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Gson gson = new Gson();
                    LocationsResponseData locationsResponseData = gson.fromJson(response, LocationsResponseData.class);
                    Utils.hideProgressDialog();
                    switch (locationsResponseData.getSuccess()) {

                        case "1": // response is success
                            alLocatiosns.clear();
                            alLocatiosns.addAll(locationsResponseData.getData());
                            adapterLocations.notifyDataSetChanged();
                            break;
                        case "0":
                        default:
                            break;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    Toast.makeText(LocationsActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
