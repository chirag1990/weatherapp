package uk.co.chiragtailor.weatherapp.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import uk.co.chiragtailor.weatherapp.Models.LocationsResponse;
import uk.co.chiragtailor.weatherapp.R;

public class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.ViewHolder> {

    Context mContext;
    ArrayList<LocationsResponse> alLocations = new ArrayList<LocationsResponse>();
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public LocationsAdapter(Context context, ArrayList<LocationsResponse> alLocations, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.alLocations = alLocations;
        mOnItemClickListener = onItemClickListener;
    }

    /* Provide a reference to the views for each data item
     Complex data items may need more than one view per item, and
     you provide access to all the views for a data item in a view holder*/
    public class ViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case
//        public SelectableRoundedImageView iv_notification;

        public TextView tvLocationsName;
        public ImageView ivLocationsDelete;
        public LinearLayout llLocations;

        public ViewHolder(final View itemView) {
            super(itemView);

            tvLocationsName = (TextView) itemView.findViewById(R.id.tvLocationsName);
            ivLocationsDelete = (ImageView) itemView.findViewById(R.id.ivLocationsDelete);
            llLocations = (LinearLayout) itemView.findViewById(R.id.llLocations);

            //For Fire event on Whole view ......
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Nimesh Hirpara", Toast.LENGTH_SHORT).show();
                }
            });*/
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_locations, parent, false);

        // set the view's size, margins, paddings and layout parameters
        //...
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //holder.iv_thumb.getLayoutParams().height = (int) (0.3 * DisplayMetricsHandler.getScreenWidth());
        //holder.iv_thumb.getLayoutParams().width = (int) (0.3 * DisplayMetricsHandler.getScreenWidth());

        holder.tvLocationsName.setText(alLocations.get(position).getName());

        holder.ivLocationsDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mOnItemClickListener.onItemClick(v, position);
            }
        });

        holder.llLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mOnItemClickListener.onItemClick(v, position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return alLocations.size();
    }
}