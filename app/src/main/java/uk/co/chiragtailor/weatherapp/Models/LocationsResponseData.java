package uk.co.chiragtailor.weatherapp.Models;

import java.util.ArrayList;

/**
 * Created by Naresh on 29-05-2017.
 */

public class LocationsResponseData extends BaseDataResponse {

    private ArrayList<LocationsResponse> data;


    public ArrayList<LocationsResponse> getData() {
        return data;
    }

    public void setData(ArrayList<LocationsResponse> data) {
        this.data = data;
    }
}
