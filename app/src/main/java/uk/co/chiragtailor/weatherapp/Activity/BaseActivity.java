package uk.co.chiragtailor.weatherapp.Activity;

import android.support.v7.app.AppCompatActivity;


public abstract class BaseActivity extends AppCompatActivity {

    public abstract void findView();

    public abstract void setListeners();

    public abstract void init();
}
