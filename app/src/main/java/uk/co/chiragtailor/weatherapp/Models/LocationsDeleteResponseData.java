package uk.co.chiragtailor.weatherapp.Models;

/**
 * Created by Naresh on 29-05-2017.
 */

public class LocationsDeleteResponseData extends BaseDataResponse {

    public String ok;
    public String n;


    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }
}
