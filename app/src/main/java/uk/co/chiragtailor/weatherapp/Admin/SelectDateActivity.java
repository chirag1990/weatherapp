package uk.co.chiragtailor.weatherapp.Admin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.co.chiragtailor.weatherapp.Activity.BaseActivity;
import uk.co.chiragtailor.weatherapp.Adapter.DateAdapter;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;
import uk.co.chiragtailor.weatherapp.HelperLibrary.WeatherConfig;
import uk.co.chiragtailor.weatherapp.R;

import static uk.co.chiragtailor.weatherapp.HelperLibrary.Utils.hideProgressDialog;

public class SelectDateActivity extends BaseActivity {

    //Widgets-------------
    public RecyclerView rvLocations;
    public Toolbar toolbar;
    public TextView tvTitle;
    //--------------------

    //Variables-----------
    DateAdapter dateAdapter;
    ArrayList<String> alLocatiosns = new ArrayList<String>();
    private String locationID = "";
    public static Activity activity;
    //--------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_date);

        activity = SelectDateActivity.this;

        Bundle b = this.getIntent().getExtras();
        if (b != null) {
            try {
                locationID = b.getString("locationID");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        findView();
        setListeners();
        init();

        if (Utils.isNetworkAvailable(activity, false, false)) {
            Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
            getDateList();
        }
    }

    @Override
    public void findView() {
        rvLocations = (RecyclerView) findViewById(R.id.rvLocations);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvTitle);
    }

    @Override
    public void setListeners() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_locations, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addLocation:
                startActivity(new Intent(activity, NewWeatherDataActivity.class).putExtra("locationID", locationID).putExtra("date", ""));
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //


    @Override
    public void init() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvTitle.setText(getString(R.string.select_date));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        activity = SelectDateActivity.this;
        rvLocations.setLayoutManager(new LinearLayoutManager(rvLocations.getContext()));
        rvLocations.setItemAnimator(new DefaultItemAnimator());
        rvLocations.setHasFixedSize(true);

        dateAdapter = new DateAdapter(activity, alLocatiosns, locationID);
        rvLocations.setAdapter(dateAdapter);
    }

    private void getDateList() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlSelectDate;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    hideProgressDialog();
                    try {
                        JSONObject jobj = new JSONObject(response);

                        if (jobj.getInt("success") == 1) {
                            JSONArray dataArray = jobj.getJSONArray("data");
                            Log.e("!_@@ Lenth :: ", dataArray.length() + "");
                            if (dataArray.length() > 0) {
                                for (int i = 0; i < dataArray.length(); i++) {
                                    alLocatiosns.add(dataArray.get(i).toString());
                                }

                                if (alLocatiosns.size() > 0) {
                                    rvLocations.setVisibility(View.VISIBLE);
                                    rvLocations.setAdapter(dateAdapter);
                                } else {
                                    rvLocations.setVisibility(View.GONE);
                                }
                            }
                            else {
                                startActivity(new Intent(activity, NewWeatherDataActivity.class).putExtra("locationID", locationID).putExtra("date", ""));
                                finish();
                            }
                        } else {
                            Utils.showAlert(activity, getResources().getString(R.string.app_name), jobj.getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("error..", ": " + e);
                        hideProgressDialog();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("locationId", locationID);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showToastMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}
