package uk.co.chiragtailor.weatherapp.View;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import uk.co.chiragtailor.weatherapp.HelperLibrary.Constants;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;


public class CustomBoldButtonView extends AppCompatButton {
    String typeFace = Constants.opensansBoldFont;

    public CustomBoldButtonView(Context context) {
        super(context);

        if (!isInEditMode() && !TextUtils.isEmpty(typeFace)) {
            setFont();
        }
    }

    public CustomBoldButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        /*TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TextViewTypeFace, 0, 0);
       
        a.recycle();*/
        if (!isInEditMode() && !TextUtils.isEmpty(typeFace)) {
            setFont();
        }
    }

    public CustomBoldButtonView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        /*TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TextViewTypeFace, 0, 0);
        
        a.recycle();*/
        if (!isInEditMode() && !TextUtils.isEmpty(typeFace)) {
            setFont();
        }
    }

    private void setFont() {
        this.setTypeface(Utils.SetCustomFont(typeFace, getContext()));
    }
}
