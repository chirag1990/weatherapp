package uk.co.chiragtailor.weatherapp.Models;

/**
 * Created by Naresh on 30-05-2017.
 */

public class AddLocationResponseData extends BaseDataResponse {

    public LocationsResponse data;

    public LocationsResponse getData() {
        return data;
    }

    public void setData(LocationsResponse data) {
        this.data = data;
    }
}
