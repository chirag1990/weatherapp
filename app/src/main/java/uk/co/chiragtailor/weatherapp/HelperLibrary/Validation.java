package uk.co.chiragtailor.weatherapp.HelperLibrary;

import java.util.regex.Pattern;

public class Validation {

    public static boolean isValidEmail(String email) {

        if (email.equals("")) {
            return false;
        } else {
            if (Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches())
                return true;
            else return false;
        }
    }

    public static boolean isPasswordValid(String password) {
        return password.length() > 1;
    }

}
