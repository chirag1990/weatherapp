package uk.co.chiragtailor.weatherapp.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

import uk.co.chiragtailor.weatherapp.R;
import uk.co.chiragtailor.weatherapp.User.WeatherAppUserMainActivity;

public class SplashActivity extends AppCompatActivity {


    //Widgets--------------------------
    //---------------------------------

    //Variable-------------------------
    private static final int timerConst = 3000;
    //---------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        startAnimationForLogo();
        setTimerForSplash();
    }

    private void setTimerForSplash() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), WeatherAppUserMainActivity.class));
                finish();
            }
        }, timerConst);
    }

    private void startAnimationForLogo() {
        ImageView mImageView = (ImageView) findViewById(R.id.splashLogoImageView);
        // Scaling
        Animation scale = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        // 1 second duration
        scale.setDuration(1000);

        // Animation set to join both scaling and moving
        AnimationSet animSet = new AnimationSet(true);
        animSet.setFillEnabled(true);
        animSet.addAnimation(scale);
        // Launching animation set
        mImageView.startAnimation(animSet);
    }
}
