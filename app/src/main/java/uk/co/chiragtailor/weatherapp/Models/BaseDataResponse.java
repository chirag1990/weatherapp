package uk.co.chiragtailor.weatherapp.Models;

/**
 * Created by Naresh on 29-05-2017.
 */

public class BaseDataResponse {

    public String success;
    public String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
