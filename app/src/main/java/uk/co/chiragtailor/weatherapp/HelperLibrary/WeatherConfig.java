package uk.co.chiragtailor.weatherapp.HelperLibrary;

/**
 * Created by Chirag on 01/05/2017.
 */

public class WeatherConfig {
    //public static final String url = "http://192.168.0.5:3001";
    public static final String url = "http://weather.chiragtailor.co.uk";
    public static final String urlLogin = "/v1/users/login";
    public static final String urlLocations = "/v1/locations";
    public static final String urlNewWhether = "/v1/weather-data";
    public static final String urlDeleteWhether = "/v1/weather-data/location";
    public static final String urlTemprature = "/v1/weather-data/location";
    public static final String urlSelectDate = "/v1/weather-data/getByDate";
}
