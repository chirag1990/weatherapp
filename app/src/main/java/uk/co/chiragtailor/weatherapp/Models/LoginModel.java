package uk.co.chiragtailor.weatherapp.Models;

import java.io.Serializable;

/**
 * Created by Chirag on 01/05/2017.
 */

public class LoginModel implements Serializable {

    private String success;
    private String message;
    private userLoginResponse userLoginResponse;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginModel.userLoginResponse getUserLoginResponse() {
        return userLoginResponse;
    }

    public void setUserLoginResponse(LoginModel.userLoginResponse userLoginResponse) {
        this.userLoginResponse = userLoginResponse;
    }

    public class userLoginResponse implements Serializable {
        private String password;
        private String email;
        private String name;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
