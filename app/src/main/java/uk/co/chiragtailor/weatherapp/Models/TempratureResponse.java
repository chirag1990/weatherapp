package uk.co.chiragtailor.weatherapp.Models;

import java.util.ArrayList;

/**
 * Created by Naresh on 31-05-2017.
 */

public class TempratureResponse {


    public ArrayList<Temprature> main;

    public ArrayList<Temprature> getMain() {
        return main;
    }

    public void setMain(ArrayList<Temprature> main) {
        this.main = main;
    }

    public class Temprature {

        public String _id;
        public String hourlyWeather;
        public String temp;
        public String precipitation;
        public String locationId;


        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getHourlyWeather() {
            return hourlyWeather;
        }

        public void setHourlyWeather(String hourlyWeather) {
            this.hourlyWeather = hourlyWeather;
        }

        public String getTemp() {
            return temp;
        }

        public void setTemp(String temp) {
            this.temp = temp;
        }

        public String getPrecipitation() {
            return precipitation;
        }

        public void setPrecipitation(String precipitation) {
            this.precipitation = precipitation;
        }

        public String getLocationId() {
            return locationId;
        }

        public void setLocationId(String locationId) {
            this.locationId = locationId;
        }
    }

}
