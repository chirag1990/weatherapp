package uk.co.chiragtailor.weatherapp.User;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.co.chiragtailor.weatherapp.Activity.BaseActivity;
import uk.co.chiragtailor.weatherapp.Admin.AdminLoginActivity;
import uk.co.chiragtailor.weatherapp.Admin.LocationsActivity;
import uk.co.chiragtailor.weatherapp.Fragment.FragmentLocationOne;
import uk.co.chiragtailor.weatherapp.Fragment.FragmentLocationThree;
import uk.co.chiragtailor.weatherapp.Fragment.FragmentLocationTwo;
import uk.co.chiragtailor.weatherapp.HelperLibrary.PreferenceManager;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;
import uk.co.chiragtailor.weatherapp.HelperLibrary.WeatherConfig;
import uk.co.chiragtailor.weatherapp.Models.LocationsResponse;
import uk.co.chiragtailor.weatherapp.Models.LocationsResponseData;
import uk.co.chiragtailor.weatherapp.R;

public class WeatherAppUserMainActivity extends BaseActivity implements View.OnClickListener {

    //Widgets--------------------------
    public Toolbar toolbar;
    public TextView tvTitle;
    public LinearLayout llLocations1, llLocations2, llLocations3;
    public TextView tvLocationsName1, tvLocationsName2, tvLocationsName3;
    public CheckBox cbLocationsExpand1, cbLocationsExpand2, cbLocationsExpand3;
    public LinearLayout llLocationsFragment1, llLocationsFragment2, llLocationsFragment3;
    //---------------------------------

    //Variable-------------------------
    PreferenceManager preferenceManager;
    Activity activity;
    ArrayList<LocationsResponse> alLocatiosns = new ArrayList<>();
    Animation animationUtils;
    public Fragment fragment = null;
    //---------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_app_user_main);

        findView();
        setListeners();
        init();

        if (Utils.isNetworkAvailable(activity, false, false)) {
            Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
            getLocationsList();
        }
    }

    @Override
    public void findView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvTitle);

        llLocations1 = (LinearLayout) findViewById(R.id.llLocations1);
        llLocations2 = (LinearLayout) findViewById(R.id.llLocations2);
        llLocations3 = (LinearLayout) findViewById(R.id.llLocations3);

        llLocationsFragment1 = (LinearLayout) findViewById(R.id.llLocationsFragment1);
        llLocationsFragment2 = (LinearLayout) findViewById(R.id.llLocationsFragment2);
        llLocationsFragment3 = (LinearLayout) findViewById(R.id.llLocationsFragment3);

        tvLocationsName1 = (TextView) findViewById(R.id.tvLocationsName1);
        tvLocationsName2 = (TextView) findViewById(R.id.tvLocationsName2);
        tvLocationsName3 = (TextView) findViewById(R.id.tvLocationsName3);

        cbLocationsExpand1 = (CheckBox) findViewById(R.id.cbLocationsExpand1);
        cbLocationsExpand2 = (CheckBox) findViewById(R.id.cbLocationsExpand2);
        cbLocationsExpand3 = (CheckBox) findViewById(R.id.cbLocationsExpand3);

    }

    @Override
    public void setListeners() {

        llLocations1.setOnClickListener(this);
        llLocations2.setOnClickListener(this);
        llLocations3.setOnClickListener(this);

    }

    @Override
    public void init() {

        activity = WeatherAppUserMainActivity.this;

        preferenceManager = new PreferenceManager(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvTitle.setText(getString(R.string.user));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.admin:

                if (preferenceManager.isUserLoggedOut()) {
                    loadAdminLogin();
                } else {
                    loadAdminScreen();
                }
        }
        return true;
    }

    private void loadAdminLogin() {
        Intent intent = new Intent(getApplicationContext(), AdminLoginActivity.class);
        startActivity(intent);
    }

    private void loadAdminScreen() {
        Intent intent = new Intent(getApplicationContext(), LocationsActivity.class);
        startActivity(intent);
    }

    private void getLocationsList() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlLocations;

            StringRequest stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    Utils.hideProgressDialog();
                    Log.e("Response ", response);

                    Gson gson = new Gson();
                    LocationsResponseData locationsResponseData = gson.fromJson(response, LocationsResponseData.class);

                    switch (locationsResponseData.getSuccess()) {

                        case "1": // response is success
                            alLocatiosns.clear();
                            alLocatiosns.addAll(locationsResponseData.getData());

                            tvLocationsName1.setText(alLocatiosns.get(0).getName());
                            tvLocationsName2.setText(alLocatiosns.get(1).getName());
                            tvLocationsName3.setText(alLocatiosns.get(2).getName());

                            break;
                        case "0":
                        default:
                            break;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showToastMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.llLocations1:
                if (cbLocationsExpand1.isChecked()) {
                    cbLocationsExpand1.setChecked(false);
                } else {
                    cbLocationsExpand1.setChecked(true);
                }

                if (llLocationsFragment1.isShown()) {
                    FilterLayoutHide(llLocationsFragment1);
                } else {
                    FilterLayoutShow(llLocationsFragment1);
                    setFragmentView(0, R.id.content_frame1);
                }

                break;

            case R.id.llLocations2:
                if (cbLocationsExpand2.isChecked()) {
                    cbLocationsExpand2.setChecked(false);
                } else {
                    cbLocationsExpand2.setChecked(true);
                }

                if (llLocationsFragment2.isShown()) {
                    FilterLayoutHide(llLocationsFragment2);
                } else {
                    FilterLayoutShow(llLocationsFragment2);
                    setFragmentView(1, R.id.content_frame2);
                }
                break;

            case R.id.llLocations3:

                if (cbLocationsExpand3.isChecked()) {
                    cbLocationsExpand3.setChecked(false);
                } else {
                    cbLocationsExpand3.setChecked(true);
                }

                if (llLocationsFragment3.isShown()) {
                    FilterLayoutHide(llLocationsFragment3);
                } else {
                    FilterLayoutShow(llLocationsFragment3);
                    setFragmentView(2, R.id.content_frame3);
                }
                break;
        }

    }

    private void FilterLayoutHide(final LinearLayout linearLayout) {

        if (linearLayout.isShown()) {
            animationUtils = AnimationUtils.loadAnimation(this, R.anim.slide_up_scale);
            linearLayout.startAnimation(animationUtils);

            animationUtils.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    linearLayout.setVisibility(View.GONE);
                    linearLayout.clearAnimation();
                }
            });
        }
    }

    private void FilterLayoutShow(LinearLayout linearLayout) {
        animationUtils = AnimationUtils.loadAnimation(this, R.anim.slide_down_scale);
        linearLayout.startAnimation(animationUtils);
        linearLayout.setVisibility(View.VISIBLE);
    }

    public void setFragmentView(int position, int frame) {

        switch (position) {
            case 0:
                fragment = new FragmentLocationOne().newInstance(alLocatiosns.get(0).get_id());
                break;
            case 1:
                fragment = new FragmentLocationTwo().newInstance(alLocatiosns.get(1).get_id());
                break;
            case 2:
                fragment = new FragmentLocationThree().newInstance(alLocatiosns.get(2).get_id());
                break;
        }

        if (fragment != null) {
            if (!isFinishing()) {
                try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().addToBackStack(null).replace(frame, fragment).commitAllowingStateLoss();
                } catch (Exception e) {
                }
            }
        }
    }

    private void showToastMsg(String msg) {
        Toast toastMsg = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
        toastMsg.show();
    }
}
