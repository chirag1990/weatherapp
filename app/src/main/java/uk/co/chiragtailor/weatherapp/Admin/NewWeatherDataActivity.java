package uk.co.chiragtailor.weatherapp.Admin;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.annotation.NotThreadSafe;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpDelete;
import cz.msebera.android.httpclient.client.methods.HttpEntityEnclosingRequestBase;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import uk.co.chiragtailor.weatherapp.Activity.BaseActivity;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Constants;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;
import uk.co.chiragtailor.weatherapp.HelperLibrary.WeatherConfig;
import uk.co.chiragtailor.weatherapp.R;

import static uk.co.chiragtailor.weatherapp.HelperLibrary.Utils.ShowProgressDialog;
import static uk.co.chiragtailor.weatherapp.HelperLibrary.Utils.hideProgressDialog;

public class NewWeatherDataActivity extends BaseActivity implements View.OnClickListener {

    //Widgets-------------------
    public TextView tvDate;
    public EditText etTime1, etTime2, etTime3, etTime4, etTime5, etTime6, etTime7, etTime8, etTime9;
    public EditText etTemp1, etTemp2, etTemp3, etTemp4, etTemp5, etTemp6, etTemp7, etTemp8, etTemp9;
    public Button btnPre1, btnPre2, btnPre3, btnPre4, btnPre5, btnPre6, btnPre7, btnPre8, btnPre9;
    public Button btnSaveWeather;
    public Toolbar toolbar;
    public TextView tvTitle;
    //--------------------------

    //Variables-----------------
    private String locationID = "", date = "";
    private Calendar calendar;
    private int mYear, month, day;
    private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    private String mTimes[] = new String[]{"00:00:00", "03:00:00", "06:00:00", "09:00:00", "12:00:00", "15:00:00", "18:00:00", "21:00:00", "23:59:59"};
    private JSONArray jsonArrayNewWeather;
    Activity activity;
    private String selected_date = "", button_date = "";
    private boolean isButtonDate = true;
    //--------------------------

    private boolean isAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_weather_data);

        findView();
        setListeners();
        init();
    }

    @Override
    public void findView() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvTitle);

        tvDate = (TextView) findViewById(R.id.tvDate);

        etTime1 = (EditText) findViewById(R.id.etTime1);
        etTime2 = (EditText) findViewById(R.id.etTime2);
        etTime3 = (EditText) findViewById(R.id.etTime3);
        etTime4 = (EditText) findViewById(R.id.etTime4);
        etTime5 = (EditText) findViewById(R.id.etTime5);
        etTime6 = (EditText) findViewById(R.id.etTime6);
        etTime7 = (EditText) findViewById(R.id.etTime7);
        etTime8 = (EditText) findViewById(R.id.etTime8);
        etTime9 = (EditText) findViewById(R.id.etTime9);

        etTemp1 = (EditText) findViewById(R.id.etTemp1);
        etTemp2 = (EditText) findViewById(R.id.etTemp2);
        etTemp3 = (EditText) findViewById(R.id.etTemp3);
        etTemp4 = (EditText) findViewById(R.id.etTemp4);
        etTemp5 = (EditText) findViewById(R.id.etTemp5);
        etTemp6 = (EditText) findViewById(R.id.etTemp6);
        etTemp7 = (EditText) findViewById(R.id.etTemp7);
        etTemp8 = (EditText) findViewById(R.id.etTemp8);
        etTemp9 = (EditText) findViewById(R.id.etTemp9);

        btnPre1 = (Button) findViewById(R.id.btnPre1);
        btnPre2 = (Button) findViewById(R.id.btnPre2);
        btnPre3 = (Button) findViewById(R.id.btnPre3);
        btnPre4 = (Button) findViewById(R.id.btnPre4);
        btnPre5 = (Button) findViewById(R.id.btnPre5);
        btnPre6 = (Button) findViewById(R.id.btnPre6);
        btnPre7 = (Button) findViewById(R.id.btnPre7);
        btnPre8 = (Button) findViewById(R.id.btnPre8);
        btnPre9 = (Button) findViewById(R.id.btnPre9);

        btnSaveWeather = (Button) findViewById(R.id.btnSaveWeather);
    }

    @Override
    public void setListeners() {

        tvDate.setOnClickListener(this);
        btnSaveWeather.setOnClickListener(this);
        btnPre1.setOnClickListener(this);
        btnPre2.setOnClickListener(this);
        btnPre3.setOnClickListener(this);
        btnPre4.setOnClickListener(this);
        btnPre5.setOnClickListener(this);
        btnPre6.setOnClickListener(this);
        btnPre7.setOnClickListener(this);
        btnPre8.setOnClickListener(this);
        btnPre9.setOnClickListener(this);
    }

    @Override
    public void init() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvTitle.setText(getString(R.string.new_weather_data));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        activity = NewWeatherDataActivity.this;


        etTime1.setText(mTimes[0]);
        etTime2.setText(mTimes[1]);
        etTime3.setText(mTimes[2]);
        etTime4.setText(mTimes[3]);
        etTime5.setText(mTimes[4]);
        etTime6.setText(mTimes[5]);
        etTime7.setText(mTimes[6]);
        etTime8.setText(mTimes[7]);
        etTime9.setText(mTimes[8]);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try {
                locationID = extras.getString("locationID");
                date = extras.getString("date");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        tvDate.setText(date);
        button_date = date;

        if(!date.equalsIgnoreCase(""))
        {
            if (Utils.isNetworkAvailable(activity, true, false)) {
                new getLocationData().execute();
            }
        }
    }

    private class getLocationData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            try {

                String url = WeatherConfig.url + WeatherConfig.urlDeleteWhether;

                InputStream inputStream = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);

                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("date", date);
                jsonObject1.put("locationId", locationID);

                StringEntity se = new StringEntity(jsonObject1.toString());
                httpPost.setEntity(se);

                httpPost.setHeader("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                httpPost.setHeader("Content-type", "application/json");
                HttpResponse httpResponse = httpclient.execute(httpPost);
                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null) {
                    result = Utils.convertInputStreamToString(inputStream);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
            Log.e("", "result : " + result);

            try {
                if (!result.equalsIgnoreCase("")) {
                    try {
                        JSONObject resultObj = new JSONObject(result);

                        if (resultObj.getInt("success") == 1) {
                            Utils.showAlert(activity, getString(R.string.app_name), resultObj.get("message").toString());
                            JSONObject data_obj = resultObj.getJSONObject("data");
                            JSONArray dataArray = data_obj.getJSONArray("main");
                            Log.e("!_@@ Lenth :: ", dataArray.length() + "");
                            if (dataArray.length() > 0) {
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject jobj = dataArray.getJSONObject(i);
                                    if (i == 0) {
                                        etTemp1.setText("" + jobj.getInt("temp"));
                                        btnPre1.setText(jobj.getString("precipitation"));
                                    } else if (i == 1) {
                                        etTemp2.setText("" + jobj.getInt("temp"));
                                        btnPre2.setText(jobj.getString("precipitation"));
                                    } else if (i == 2) {
                                        etTemp3.setText("" + jobj.getInt("temp"));
                                        btnPre3.setText(jobj.getString("precipitation"));
                                    } else if (i == 3) {
                                        etTemp4.setText("" + jobj.getInt("temp"));
                                        btnPre4.setText(jobj.getString("precipitation"));
                                    } else if (i == 4) {
                                        etTemp5.setText("" + jobj.getInt("temp"));
                                        btnPre5.setText(jobj.getString("precipitation"));
                                    } else if (i == 5) {
                                        etTemp6.setText("" + jobj.getInt("temp"));
                                        btnPre6.setText(jobj.getString("precipitation"));
                                    } else if (i == 6) {
                                        etTemp7.setText("" + jobj.getInt("temp"));
                                        btnPre7.setText(jobj.getString("precipitation"));
                                    } else if (i == 7) {
                                        etTemp8.setText("" + jobj.getInt("temp"));
                                        btnPre8.setText(jobj.getString("precipitation"));
                                    } else if (i == 8) {
                                        etTemp9.setText("" + jobj.getInt("temp"));
                                        btnPre9.setText(jobj.getString("precipitation"));
                                    }
                                }
                            }
                        } else {
                            Utils.showAlert(activity, getString(R.string.app_name), resultObj.get("message").toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //Utils.showAlert(activity, getString(R.string.error), getString(R.string.error));
                }
            } catch (Exception e) {

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_weather_delete, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.deleteLocation:
                new MaterialDialog.Builder(this)
                        .title("Delete Weather")
                        .inputType(InputType.TYPE_CLASS_DATETIME)
                        .input(R.string.delete_weather_hint, R.string.blank, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                if (input.length() > 0) {
                                    if (Utils.isNetworkAvailable(activity, false, false)) {
                                        selected_date = input.toString();
                                        isAdd = false;
                                        isButtonDate = false;
                                        ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
                                        new deleteWeatherData().execute();
                                    } else {
                                        showToastMessage(getResources().getString(R.string.network_alert));
                                    }
                                }
                            }
                        }).show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class deleteWeatherData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {

            String result = "";
            try {
                JSONObject jsonObject1 = new JSONObject();
                if (isButtonDate)
                    jsonObject1.put("date", button_date);
                else
                    jsonObject1.put("date", selected_date);
                jsonObject1.put("locationId", locationID);

                String url = WeatherConfig.url + WeatherConfig.urlDeleteWhether;

                InputStream inputStream = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpDelete httpPost = new HttpDelete(url);

                httpPost.setHeader("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                httpPost.setHeader("Content-type", "application/json");

                Log.e("", "RE : " + jsonObject1.toString());


                StringEntity se = new StringEntity(jsonObject1.toString());
                HttpDeleteWithBody httpDeleteWithBody = new HttpDeleteWithBody(url);
                httpDeleteWithBody.setHeader("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                httpDeleteWithBody.setHeader("Content-type", "application/json");
                httpDeleteWithBody.setEntity(se);

                HttpResponse response = httpclient.execute(httpDeleteWithBody);
                inputStream = response.getEntity().getContent();
                if (inputStream != null) {
                    result = Utils.convertInputStreamToString(inputStream);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            isButtonDate = true;

            if (!isAdd) {
                hideProgressDialog();
                try {
                    if (!result.equalsIgnoreCase("")) {
                        try {
                            JSONObject resultObj = new JSONObject(result);
                            if (resultObj.has("success")) {
                                showToastMessage(resultObj.get("message").toString());
                            } else {
                                showToastMessage(resultObj.get("message").toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (isAdd)
                new addWeatherData().execute();


            Log.e("", "result : " + result);


        }
    }

    @NotThreadSafe
    class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {

        public static final String METHOD_NAME = "DELETE";

        public String getMethod() {
            return METHOD_NAME;
        }

        public HttpDeleteWithBody(final String uri) {
            super();
            setURI(URI.create(uri));
        }

        public HttpDeleteWithBody(final URI uri) {
            super();
            setURI(uri);
        }

        public HttpDeleteWithBody() {
            super();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.tvDate:
                pickDate();
                break;
            case R.id.btnPre1:
                selectWeatherType(btnPre1);
                break;
            case R.id.btnPre2:
                selectWeatherType(btnPre2);
                break;
            case R.id.btnPre3:
                selectWeatherType(btnPre3);
                break;
            case R.id.btnPre4:
                selectWeatherType(btnPre4);
                break;
            case R.id.btnPre5:
                selectWeatherType(btnPre5);
                break;
            case R.id.btnPre6:
                selectWeatherType(btnPre6);
                break;
            case R.id.btnPre7:
                selectWeatherType(btnPre7);
                break;
            case R.id.btnPre8:
                selectWeatherType(btnPre8);
                break;
            case R.id.btnPre9:
                selectWeatherType(btnPre9);
                break;
            case R.id.btnSaveWeather:

                try {

                    if (Utils.isEmpty(etTemp1.getText().toString().trim()) || etTemp1.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));
                    else if (Utils.isEmpty(etTemp2.getText().toString().trim()) || etTemp2.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));
                    else if (Utils.isEmpty(etTemp3.getText().toString().trim()) || etTemp3.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));
                    else if (Utils.isEmpty(etTemp4.getText().toString().trim()) || etTemp4.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));
                    else if (Utils.isEmpty(etTemp5.getText().toString().trim()) || etTemp5.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));
                    else if (Utils.isEmpty(etTemp6.getText().toString().trim()) || etTemp6.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));
                    else if (Utils.isEmpty(etTemp7.getText().toString().trim()) || etTemp7.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));
                    else if (Utils.isEmpty(etTemp8.getText().toString().trim()) || etTemp8.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));
                    else if (Utils.isEmpty(etTemp9.getText().toString().trim()) || etTemp9.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_temp));


                    else if (Utils.isEmpty(btnPre1.getText().toString().trim()) || btnPre1.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else if (Utils.isEmpty(btnPre2.getText().toString().trim()) || btnPre2.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else if (Utils.isEmpty(btnPre3.getText().toString().trim()) || btnPre3.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else if (Utils.isEmpty(btnPre4.getText().toString().trim()) || btnPre4.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else if (Utils.isEmpty(btnPre5.getText().toString().trim()) || btnPre5.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else if (Utils.isEmpty(btnPre6.getText().toString().trim()) || btnPre6.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else if (Utils.isEmpty(btnPre7.getText().toString().trim()) || btnPre7.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else if (Utils.isEmpty(btnPre8.getText().toString().trim()) || btnPre8.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else if (Utils.isEmpty(btnPre9.getText().toString().trim()) || btnPre9.length() <= 0)
                        Utils.showAlert(activity, getString(R.string.app_name), getString(R.string.alert_video_pre));
                    else {

                        if (Utils.isNetworkAvailable(activity, true, false)) {
                            ShowProgressDialog(activity, getString(R.string.please_wait));

                            try {
                                jsonArrayNewWeather = new JSONArray();

                                JSONObject jsonObject1 = new JSONObject();
                                jsonObject1.put(Constants.hourlyWeatherKey, getDateTime(mTimes[0]));
                                jsonObject1.put(Constants.precipitationKey, btnPre1.getText().toString().trim());
                                jsonObject1.put(Constants.tempKey, Integer.parseInt(etTemp1.getText().toString().trim()));
                                jsonObject1.put(Constants.locationIdKey, locationID);

                                JSONObject jsonObject2 = new JSONObject();
                                jsonObject2.put(Constants.hourlyWeatherKey, getDateTime(mTimes[1]));
                                jsonObject2.put(Constants.precipitationKey, btnPre2.getText().toString().trim());
                                jsonObject2.put(Constants.tempKey, Integer.parseInt(etTemp2.getText().toString().trim()));
                                jsonObject2.put(Constants.locationIdKey, locationID);

                                JSONObject jsonObject3 = new JSONObject();
                                jsonObject3.put(Constants.hourlyWeatherKey, getDateTime(mTimes[2]));
                                jsonObject3.put(Constants.precipitationKey, btnPre3.getText().toString().trim());
                                jsonObject3.put(Constants.tempKey, Integer.parseInt(etTemp3.getText().toString().trim()));
                                jsonObject3.put(Constants.locationIdKey, locationID);

                                JSONObject jsonObject4 = new JSONObject();
                                jsonObject4.put(Constants.hourlyWeatherKey, getDateTime(mTimes[3]));
                                jsonObject4.put(Constants.precipitationKey, btnPre4.getText().toString().trim());
                                jsonObject4.put(Constants.tempKey, Integer.parseInt(etTemp4.getText().toString().trim()));
                                jsonObject4.put(Constants.locationIdKey, locationID);

                                JSONObject jsonObject5 = new JSONObject();
                                jsonObject5.put(Constants.hourlyWeatherKey, getDateTime(mTimes[4]));
                                jsonObject5.put(Constants.precipitationKey, btnPre5.getText().toString().trim());
                                jsonObject5.put(Constants.tempKey, Integer.parseInt(etTemp5.getText().toString().trim()));
                                jsonObject5.put(Constants.locationIdKey, locationID);

                                JSONObject jsonObject6 = new JSONObject();
                                jsonObject6.put(Constants.hourlyWeatherKey, getDateTime(mTimes[5]));
                                jsonObject6.put(Constants.precipitationKey, btnPre6.getText().toString().trim());
                                jsonObject6.put(Constants.tempKey, Integer.parseInt(etTemp6.getText().toString().trim()));
                                jsonObject6.put(Constants.locationIdKey, locationID);

                                JSONObject jsonObject7 = new JSONObject();
                                jsonObject7.put(Constants.hourlyWeatherKey, getDateTime(mTimes[6]));
                                jsonObject7.put(Constants.precipitationKey, btnPre7.getText().toString().trim());
                                jsonObject7.put(Constants.tempKey, Integer.parseInt(etTemp7.getText().toString().trim()));
                                jsonObject7.put(Constants.locationIdKey, locationID);

                                JSONObject jsonObject8 = new JSONObject();
                                jsonObject8.put(Constants.hourlyWeatherKey, getDateTime(mTimes[7]));
                                jsonObject8.put(Constants.precipitationKey, btnPre8.getText().toString().trim());
                                jsonObject8.put(Constants.tempKey, Integer.parseInt(etTemp8.getText().toString().trim()));
                                jsonObject8.put(Constants.locationIdKey, locationID);

                                JSONObject jsonObject9 = new JSONObject();
                                jsonObject9.put(Constants.hourlyWeatherKey, getDateTime(mTimes[8]));
                                jsonObject9.put(Constants.precipitationKey, btnPre9.getText().toString().trim());
                                jsonObject9.put(Constants.tempKey, Integer.parseInt(etTemp9.getText().toString().trim()));
                                jsonObject9.put(Constants.locationIdKey, locationID);

                                jsonArrayNewWeather.put(jsonObject1);
                                jsonArrayNewWeather.put(jsonObject2);
                                jsonArrayNewWeather.put(jsonObject3);
                                jsonArrayNewWeather.put(jsonObject4);
                                jsonArrayNewWeather.put(jsonObject5);
                                jsonArrayNewWeather.put(jsonObject6);
                                jsonArrayNewWeather.put(jsonObject7);
                                jsonArrayNewWeather.put(jsonObject8);
                                jsonArrayNewWeather.put(jsonObject9);

                                Log.e("", "update json : " + jsonArrayNewWeather.toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            isAdd = true;
                            new deleteWeatherData().execute();

                        } else {
                            showToastMessage(getResources().getString(R.string.network_alert));
                        }
                    }
                } catch (Exception e) {

                }
                break;
        }
    }

    private void selectWeatherType(final Button btnPre) {

        final String[] colleges = getResources().getStringArray(R.array.weather);

        AlertDialog.Builder build1 = new AlertDialog.Builder(activity);
        build1.setTitle("Select weather type");
        build1.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        build1.setItems(colleges, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                btnPre.setText(colleges[which].toString());
            }
        }).create().show();
    }

    private void pickDate() {

        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                try {
                    day = dayOfMonth;
                    month = monthOfYear;
                    mYear = year;
                    tvDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, mYear);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DAY_OF_MONTH, day);

                    button_date = simpleDateFormat.format(calendar.getTime());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, mYear, month, day);
        datePickerDialog.show();
    }

    public String getDateTime(String mTime) {
        String s = "";
        /*Tue May 30, 2017 13:56:01 GMT+05:30*/
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = fmt.parse(button_date);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd yyyy", Locale.getDefault());
            s = simpleDateFormat.format(date);

            s = s + " " + mTime + " GMT+0000 (UTC)";

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.e("S Date-- ", "" + s);
        return s;
        //return simpleDateFormat.format(calendar.getTime());
        /*Date date = new Date();
        return simpleDateFormat.format(date);*/
    }

    public void showToastMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private class addWeatherData extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {

            String result = "";
            try {

                String url = WeatherConfig.url + WeatherConfig.urlNewWhether;

                InputStream inputStream = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);

                StringEntity se = new StringEntity(jsonArrayNewWeather.toString());
                httpPost.setEntity(se);

                httpPost.setHeader("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                httpPost.setHeader("Content-type", "application/json");
                HttpResponse httpResponse = httpclient.execute(httpPost);
                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null) {
                    result = Utils.convertInputStreamToString(inputStream);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
            isAdd = false;
            Log.e("", "result : " + result);

            try {
                if (!result.equalsIgnoreCase("")) {
                    try {
                        JSONObject resultObj = new JSONObject(result);
                        if (resultObj.has("success")) {
                            showToastMessage(resultObj.get("message").toString());
                            startActivity(new Intent(activity, SelectDateActivity.class).putExtra("locationID", locationID));
                            SelectDateActivity.activity.finish();
                            finish();
                        } else {
                            Utils.showAlert(activity, getString(R.string.app_name), resultObj.get("message").toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //Utils.showAlert(activity, getString(R.string.error), getString(R.string.error));
                }
            } catch (Exception e) {

            }
        }
    }
}
