package uk.co.chiragtailor.weatherapp.Models;

/**
 * Created by Naresh on 31-05-2017.
 */

public class TempratureResponseData extends BaseDataResponse {

    public TempratureResponse data;

    public TempratureResponse getData() {
        return data;
    }

    public void setData(TempratureResponse data) {
        this.data = data;
    }
}
