package uk.co.chiragtailor.weatherapp.Admin;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

import uk.co.chiragtailor.weatherapp.Activity.BaseActivity;
import uk.co.chiragtailor.weatherapp.HelperLibrary.PreferenceManager;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Validation;
import uk.co.chiragtailor.weatherapp.HelperLibrary.WeatherConfig;
import uk.co.chiragtailor.weatherapp.Models.LoginModel;
import uk.co.chiragtailor.weatherapp.R;

public class AdminLoginActivity extends BaseActivity {

    public EditText emailEditText, passwordEditText;
    public TextView createAccountTextView;
    public Button signInButton;
    public Toolbar toolbar;
    public TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        findView();
        setUpSignUpText();

        if (Utils.isNetworkAvailable(AdminLoginActivity.this, true, false)) {
            signInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signInButtonClick();
                }
            });
        }
    }

    @Override
    public void findView() {

        emailEditText = (EditText) findViewById(R.id.emailSignInEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordSignInEditText);
        createAccountTextView = (TextView) findViewById(R.id.createAccountTextView);
        signInButton = (Button) findViewById(R.id.signInButton);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tvTitle);

        emailEditText.setText("admin@weatherapi.com");
        passwordEditText.setText("1234");
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void init() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setUpSignUpText() {

        SpannableStringBuilder builder = new SpannableStringBuilder();
        String first = "Don't Have An Account Yet? ";
        SpannableString firstSpannable = new SpannableString(first);
        Resources res = getApplicationContext().getResources();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            firstSpannable.setSpan(new ForegroundColorSpan(res.getColor(R.color.white, null)), 0, first.length(), 0);
        } else {
            int color = ContextCompat.getColor(this, R.color.white);
            firstSpannable.setSpan(new ForegroundColorSpan(color), 0, first.length(), 0);
        }
        builder.append(firstSpannable);
        String second = "Sign Up";
        SpannableString secondSpannable = new SpannableString(second);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            secondSpannable.setSpan(new ForegroundColorSpan(res.getColor(R.color.blue, null)), 0, second.length(), 0);
        } else {
            int color = ContextCompat.getColor(this, R.color.blue);
            secondSpannable.setSpan(new ForegroundColorSpan(color), 0, second.length(), 0);
        }
        secondSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, second.length(), 0);
        builder.append(secondSpannable);

        createAccountTextView.setText(builder, TextView.BufferType.SPANNABLE);
    }

    public void signInButtonClick() {

        final String emailText = emailEditText.getText().toString().trim();
        final String passwordText = passwordEditText.getText().toString().trim();
        boolean isEmailValid, isPasswordValid;

        try {
            if (Validation.isValidEmail(emailText)) {
                isEmailValid = true;
            } else {
                if (emailText.length() == 0) {
                    emailEditText.setError("Please enter a email address");
                } else {
                    emailEditText.setError("Please enter valid a email address");
                }
                isEmailValid = false;
            }

            if (Validation.isPasswordValid(passwordText)) {
                isPasswordValid = true;
            } else {
                passwordEditText.setError("Please enter a password");
                isPasswordValid = false;
            }

            if (isEmailValid && isPasswordValid) {

                String url = WeatherConfig.url + WeatherConfig.urlLogin;

                StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.e("response", "----  " + response);

                        JsonObject jsonObject = (JsonObject) new JsonParser().parse(response);
                        JsonElement jsonElement = jsonObject.getAsJsonObject("user");
                        LoginModel.userLoginResponse object = new GsonBuilder().create().fromJson(jsonElement, LoginModel.userLoginResponse.class);

                        Gson gson = new Gson();
                        LoginModel loginResponse = gson.fromJson(response, LoginModel.class);

                        //client_id = object.getClient_id();

                        switch (loginResponse.getSuccess()) {
                            case "1": // response is success
                                saveLoginDetails(emailText, passwordText);
                                startHomeActivity();
                                Toast.makeText(AdminLoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                                break;
                            case "0":
                            default:
                                Toast.makeText(AdminLoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AdminLoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("email", emailText);
                        params.put("password", passwordText);
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startHomeActivity() {
        startActivity(new Intent(AdminLoginActivity.this, LocationsActivity.class));
        finish();
    }

    private void saveLoginDetails(String email, String password) {
        PreferenceManager pm = new PreferenceManager(this);
        pm.saveLoginDetails(email, password);
    }
}
