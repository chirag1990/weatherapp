package uk.co.chiragtailor.weatherapp.HelperLibrary;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Chirag on 25/05/2017.
 */

public class PreferenceManager {

    Context context;

    public PreferenceManager(Context context) {
        this.context = context;
    }

    public void saveLoginDetails(String email, String password) {

        Log.i("email", email);
        Log.i("password", password);
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("email", email);
        editor.putString("password", password);
        editor.commit();
    }

    public String getEmail() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("email", "");
    }

    public String getPassword() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("password", "");
    }

    public boolean isUserLoggedOut() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        boolean isEmailEmpty = sharedPreferences.getString("email", "").isEmpty();
        boolean isPasswordEmpty = sharedPreferences.getString("password", "").isEmpty();

        return isEmailEmpty || isPasswordEmpty;
    }

}
