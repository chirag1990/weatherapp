package uk.co.chiragtailor.weatherapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import uk.co.chiragtailor.weatherapp.Admin.LocationsActivity;
import uk.co.chiragtailor.weatherapp.Admin.NewWeatherDataActivity;
import uk.co.chiragtailor.weatherapp.Admin.SelectDateActivity;
import uk.co.chiragtailor.weatherapp.Models.LocationsResponse;
import uk.co.chiragtailor.weatherapp.R;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.ViewHolder> {

    Context mContext;
    ArrayList<String> alLocations = new ArrayList<String>();
    public String locationID = "";

    // Provide a suitable constructor (depends on the kind of dataset)
    public DateAdapter(Context context, ArrayList<String> alLocations, String locationID) {
        this.mContext = context;
        this.alLocations = alLocations;
        this.locationID = locationID;
    }

    /* Provide a reference to the views for each data item
     Complex data items may need more than one view per item, and
     you provide access to all the views for a data item in a view holder*/
    public class ViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case
//        public SelectableRoundedImageView iv_notification;

        public TextView tvLocationsName;
        public LinearLayout llDate;

        public ViewHolder(final View itemView) {
            super(itemView);

            tvLocationsName = (TextView) itemView.findViewById(R.id.tvLocationsName);
            llDate = (LinearLayout) itemView.findViewById(R.id.llDate);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_date, parent, false);

        // set the view's size, margins, paddings and layout parameters
        //...
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvLocationsName.setText(alLocations.get(position).toString());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("", "dd " + locationID);
                mContext.startActivity(new Intent(mContext, NewWeatherDataActivity.class).putExtra("locationID", locationID).putExtra("date", alLocations.get(position).toString()));
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return alLocations.size();
    }
}