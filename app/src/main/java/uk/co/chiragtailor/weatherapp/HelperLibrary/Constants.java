package uk.co.chiragtailor.weatherapp.HelperLibrary;

public class Constants {
    /**
     * font declaration
     */
    public static String opensansRegularFont = "fonts/opensans_regular.ttf";
    public static String opensansBoldFont = "fonts/opensans_bold.ttf";
    public static String gothamMediumFont = "fonts/gotham_medium.ttf";
    public static String robotoTtf = "fonts/roboto_regular.ttf";
    public static String robotoBoldTtf = "fonts/roboto_bold.ttf";

    public static String hourlyWeatherKey = "hourlyWeather";
    public static String precipitationKey = "precipitation";
    public static String tempKey = "temp";
    public static String locationIdKey = "locationId";

    public static String TODAY = "today";
    public static String TONIGHT = "tonight";
    public static String TOMORROW = "tomorrow";

}
