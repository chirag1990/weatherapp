package uk.co.chiragtailor.weatherapp.HelperLibrary;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.*;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import uk.co.chiragtailor.weatherapp.R;

public class Utils {

    public static ProgressDialog dialog;
    public static AlertDialog alert;
    private static String TAG = Utils.class.getSimpleName();

    //Check the internet connection
    public static boolean isNetworkAvailable(final Context context, boolean canShowErrorDialogOnFail, final boolean isFinish) {
        boolean isNetAvailable = false;
        if (context != null) {
            final ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            if (mConnectivityManager != null) {
                boolean mobileNetwork = false;
                boolean wifiNetwork = false;
                boolean mobileNetworkConnecetd = false;
                boolean wifiNetworkConnecetd = false;

                final NetworkInfo mobileInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                final NetworkInfo wifiInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (mobileInfo != null) {
                    mobileNetwork = mobileInfo.isAvailable();
                }

                if (wifiInfo != null) {
                    wifiNetwork = wifiInfo.isAvailable();
                }

                if (wifiNetwork || mobileNetwork) {
                    if (mobileInfo != null)
                        mobileNetworkConnecetd = mobileInfo
                                .isConnectedOrConnecting();
                    wifiNetworkConnecetd = wifiInfo.isConnectedOrConnecting();
                }

                isNetAvailable = (mobileNetworkConnecetd || wifiNetworkConnecetd);
            }
            context.setTheme(R.style.AppTheme);
            if (!isNetAvailable && canShowErrorDialogOnFail) {
                Log.v("TAG", "context : " + context.toString());
                if (context instanceof Activity) {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            showAlertWithFinish((Activity) context, context.getString(R.string.app_name), context.getString(R.string.network_alert), isFinish);
                        }
                    });
                }
            }
        }

        return isNetAvailable;
    }

    //Show ProgressDialog Helper
    public static void ShowProgressDialog(Activity activity, String message) {
        if (dialog != null) {
            dialog.dismiss();
        }
        try {
            dialog = new ProgressDialog(activity);
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Hide ProgressDialog Helper
    public static void hideProgressDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    //Alert helper
    public static void showAlertWithFinish(final Activity activity, String title, String message, final boolean isFinish) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isFinish) {
                    dialog.dismiss();
                    activity.finish();
                } else {
                    dialog.dismiss();
                }
            }
        }).show();
    }


    //get path of resize image
    public static String getSmallImageResizeImagePath(Activity activity, String imageUri, String name) {
        Log.e("", "Name:::: " + name);
        //Resize image
        String mPath = "";
        final File file = new File(imageUri);
        if (file.exists()) {
            Bitmap myBitmap = Utils.compressImageSmall(activity, imageUri);
            Log.e("", "size: " + myBitmap.getWidth() + "-" + myBitmap.getHeight());
            if (myBitmap != null) {
                mPath = Environment.getExternalStorageDirectory().toString() + "/" + name + ".jpeg";
                OutputStream fout = null;
                final File imageFile = new File(mPath);

                try {
                    fout = new FileOutputStream(imageFile);
                    myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fout);
                    fout.flush();
                    fout.close();
                } catch (final FileNotFoundException e) {
                    e.printStackTrace();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return mPath;
    }

    //convert bigger image to smaller
    public static Bitmap compressImageSmall(Activity activity, String path) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        Bitmap thumb = null;

        Drawable drawable = new BitmapDrawable(activity.getResources(), bitmap);
        int widthMeasureSpec = 200, heightMeasureSpec = 200, width, height;

        float imageSideRatio = (float) drawable.getIntrinsicWidth() / (float) drawable.getIntrinsicHeight();
        float viewSideRatio = (float) View.MeasureSpec.getSize(widthMeasureSpec) / (float) View.MeasureSpec.getSize(heightMeasureSpec);
        if (imageSideRatio >= viewSideRatio) {
            // Image is wider than the display (ratio)
            width = View.MeasureSpec.getSize(widthMeasureSpec);
            height = (int) (width / imageSideRatio);
            thumb = Bitmap.createScaledBitmap(bitmap, width, height, false);

            if (thumb.getHeight() < heightMeasureSpec) {
                height = View.MeasureSpec.getSize(heightMeasureSpec);
                width = (int) (height * imageSideRatio);
                thumb = Bitmap.createScaledBitmap(bitmap, width, height, false);
            }
        } else {
            // Image is taller than the display (ratio)
            height = View.MeasureSpec.getSize(heightMeasureSpec);
            width = (int) (height * imageSideRatio);
            thumb = Bitmap.createScaledBitmap(bitmap, width, height, false);

            if (thumb.getWidth() < widthMeasureSpec) {
                width = View.MeasureSpec.getSize(widthMeasureSpec);
                height = (int) (width / imageSideRatio);
                thumb = Bitmap.createScaledBitmap(bitmap, width, height, false);
            }
        }
        return thumb;
    }

    public static Typeface SetCustomFont(String fontName, Context context) {
        return Typeface.createFromAsset(context.getAssets(), fontName);
    }

    public static void showAlert(final Activity activity, String title, String message) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok), null);
        builder.show();
    }

    public static boolean isEmpty(CharSequence charSequence) {
        return TextUtils.isEmpty(charSequence) || charSequence.toString().equalsIgnoreCase("null");
    }

    public static String convertInputStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        is.close();
        return sb.toString();
    }

    public static String formatTemperature(Context context, double temperature) {
        // Data stored in Celsius by default.  If user prefers to see in Fahrenheit, convert
        // the values here.
        String suffix = "\u00B0";
        if (!isMetric(context)) {
            temperature = (temperature * 1.8) + 32;
        }

        // For presentation, assume the user doesn't care about tenths of a degree.
        return String.format(context.getString(R.string.format_temperature), temperature);
    }

    public static boolean isMetric(Context context) {
        SharedPreferences prefs = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.pref_units_key),
                context.getString(R.string.pref_units_metric))
                .equals(context.getString(R.string.pref_units_metric));
    }

    public static String getDateFormat(String hourlyWeather) {

        String temp = hourlyWeather.replace("T", " ");
        String[] separated = temp.split("\\.");

        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("HH:mm");
        try {
            Date oneWayTripDate = input.parse(separated[0]);
            return output.format(oneWayTripDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getSepareteDateToday(String hourlyWeather) {

        String[] separated = hourlyWeather.split("T");
        String s = separated[1].substring(0, 2);

        int time = Integer.parseInt(s);

        if (time >= 6 && time <= 18) {
            Log.e("Date", "today " + s);
            return Constants.TODAY;
        } else if (time > 18) {
            Log.e("Date", "tonight " + s);
            return Constants.TONIGHT;
        }
        return s;
    }

    public static String getSepareteDateTomorrow(String hourlyWeather) {

        String[] separated = hourlyWeather.split("T");
        String s = separated[1].substring(0, 2);

        int time = Integer.parseInt(s);

        if (time >= 0 && time < 6) {
            Log.e("Date", "tonight " + s);
            return Constants.TONIGHT;
        } else if (time >= 6 && time <= 18) {
            Log.e("Date", "tomorrow " + s);
            return Constants.TOMORROW;
        }
        return s;
    }
}
