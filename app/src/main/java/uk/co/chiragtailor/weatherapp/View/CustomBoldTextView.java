package uk.co.chiragtailor.weatherapp.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import uk.co.chiragtailor.weatherapp.HelperLibrary.Constants;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;


public class CustomBoldTextView extends TextView {
    String typeFace = Constants.gothamMediumFont;

    public CustomBoldTextView(Context context) {
        super(context);

        if (!isInEditMode() && !TextUtils.isEmpty(typeFace)) {
            setFont();
        }
    }

    @SuppressLint("Recycle")
    public CustomBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
       /* TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TextViewTypeFace, 0, 0);
        a.recycle();*/
        if (!isInEditMode() && !TextUtils.isEmpty(typeFace)) {
            setFont();
        }
    }

    public CustomBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        /*TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TextViewTypeFace, 0, 0);
        a.recycle();*/
        if (!isInEditMode() && !TextUtils.isEmpty(typeFace)) {
            setFont();
        }
    }

    private void setFont() {
        this.setTypeface(Utils.SetCustomFont(typeFace, getContext()));
    }
}
