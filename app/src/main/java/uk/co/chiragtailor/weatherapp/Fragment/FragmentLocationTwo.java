package uk.co.chiragtailor.weatherapp.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import uk.co.chiragtailor.weatherapp.Adapter.LocationsAdapterDays;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Constants;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;
import uk.co.chiragtailor.weatherapp.HelperLibrary.WeatherConfig;
import uk.co.chiragtailor.weatherapp.Models.TempratureResponse;
import uk.co.chiragtailor.weatherapp.Models.TempratureResponseData;
import uk.co.chiragtailor.weatherapp.R;

public class FragmentLocationTwo extends Fragment implements View.OnClickListener {

    //Android Widgets---------------------------------------
    public TabLayout tabLayoutDays, tabsLayoutDaily;
    RecyclerView rvLocationsDays1, rvLocationsDays2, rvLocationsDays3, rvLocationsDays4;
    RecyclerView rvLocationsDailyToday, rvLocationsDailyTonight, rvLocationsDailyTomorrow;
    LinearLayout llDay1, llDay2, llDay3, llDay4;
    LinearLayout llDaily1, llDaily2, llDaily3;

    //Variable----------------------------------------------
    View viewHomeFragment;
    private AppCompatActivity activity;
    public String locationId = "";
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    String mTabsTitleDays[] = new String[]{"Day1", "Day2", "Day3", "Day4"};
    String mTabsTitleDaily[] = new String[]{"Today", "Tonight", "Tomorrow"};

    LocationsAdapterDays adapterLocationsDays1;
    ArrayList<TempratureResponse.Temprature> alTempraturesDays1 = new ArrayList<>();

    LocationsAdapterDays adapterLocationsDays2;
    ArrayList<TempratureResponse.Temprature> alTempraturesDays2 = new ArrayList<>();

    LocationsAdapterDays adapterLocationsDays3;
    ArrayList<TempratureResponse.Temprature> alTempraturesDays3 = new ArrayList<>();

    LocationsAdapterDays adapterLocationsDays4;
    ArrayList<TempratureResponse.Temprature> alTempraturesDays4 = new ArrayList<>();


    ArrayList<TempratureResponse.Temprature> alTempraturesDailyOne = new ArrayList<>();
    ArrayList<TempratureResponse.Temprature> alTempraturesDailyTwo = new ArrayList<>();


    LocationsAdapterDays adapterLocationsDailyToday;
    ArrayList<TempratureResponse.Temprature> alTempraturesDailyToday = new ArrayList<>();

    LocationsAdapterDays adapterLocationsDailyTonight;
    ArrayList<TempratureResponse.Temprature> alTempraturesDailyTomorrow = new ArrayList<>();

    LocationsAdapterDays adapterLocationsDailyTomorrow;
    ArrayList<TempratureResponse.Temprature> alTempraturesDailyTonight = new ArrayList<>();

    int daysToAdd = 0;
    private Calendar calendar;
    //------------------------------------------------------

    public FragmentLocationTwo() {
    }

    public static FragmentLocationTwo newInstance(String locationId) {

        Bundle args = new Bundle();
        args.putString("locationId", locationId);

        FragmentLocationTwo fragment = new FragmentLocationTwo();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getArguments();
        if (extras != null) {
            try {
                locationId = extras.getString("locationId");
            } catch (Exception e) {
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewHomeFragment = inflater.inflate(R.layout.fragment_location_two, container, false);

        findResources();
        setListeners();
        init();

       /* if (Utils.isNetworkAvailable(activity, false, false)) {
            Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
            getLocationsList();
            daysToAdd = 0;
        } else {
            showToastMessage(getResources().getString(R.string.network_alert));
        }*/

        return viewHomeFragment;
    }

    private void findResources() {

        tabLayoutDays = (TabLayout) viewHomeFragment.findViewById(R.id.tabsDays);
        tabsLayoutDaily = (TabLayout) viewHomeFragment.findViewById(R.id.tabsDaily);

        rvLocationsDays1 = (RecyclerView) viewHomeFragment.findViewById(R.id.rvLocationsDays1);
        rvLocationsDays2 = (RecyclerView) viewHomeFragment.findViewById(R.id.rvLocationsDays2);
        rvLocationsDays3 = (RecyclerView) viewHomeFragment.findViewById(R.id.rvLocationsDays3);
        rvLocationsDays4 = (RecyclerView) viewHomeFragment.findViewById(R.id.rvLocationsDays4);

        rvLocationsDailyToday = (RecyclerView) viewHomeFragment.findViewById(R.id.rvLocationsDaily1);
        rvLocationsDailyTonight = (RecyclerView) viewHomeFragment.findViewById(R.id.rvLocationsDaily2);
        rvLocationsDailyTomorrow = (RecyclerView) viewHomeFragment.findViewById(R.id.rvLocationsDaily3);

        llDay1 = (LinearLayout) viewHomeFragment.findViewById(R.id.llDay1);
        llDay2 = (LinearLayout) viewHomeFragment.findViewById(R.id.llDay2);
        llDay3 = (LinearLayout) viewHomeFragment.findViewById(R.id.llDay3);
        llDay4 = (LinearLayout) viewHomeFragment.findViewById(R.id.llDay4);

        llDaily1 = (LinearLayout) viewHomeFragment.findViewById(R.id.llDaily1);
        llDaily2 = (LinearLayout) viewHomeFragment.findViewById(R.id.llDaily2);
        llDaily3 = (LinearLayout) viewHomeFragment.findViewById(R.id.llDaily3);

    }

    private void setListeners() {

        tabLayoutDays.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getText().toString()) {
                    case "Day1":

                        daysToAdd = 0;

                        if (alTempraturesDays1 != null && alTempraturesDays1.size() == 0) {

                            if (Utils.isNetworkAvailable(activity, false, false)) {
                                Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
                                getLocationsListDays1();
                            } else {
                                showToastMessage(getResources().getString(R.string.network_alert));
                            }
                        }

                        llDay1.setVisibility(View.VISIBLE);
                        llDay2.setVisibility(View.GONE);
                        llDay3.setVisibility(View.GONE);
                        llDay4.setVisibility(View.GONE);

                        break;
                    case "Day2":

                        daysToAdd = 1;

                        llDay1.setVisibility(View.GONE);
                        llDay2.setVisibility(View.VISIBLE);
                        llDay3.setVisibility(View.GONE);
                        llDay4.setVisibility(View.GONE);

                        if (alTempraturesDays2 != null && alTempraturesDays2.size() == 0) {

                            if (Utils.isNetworkAvailable(activity, false, false)) {
                                Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
                                getLocationsListDays2();
                            } else {
                                showToastMessage(getResources().getString(R.string.network_alert));
                            }
                        }
                        break;
                    case "Day3":

                        daysToAdd = 2;

                        llDay1.setVisibility(View.GONE);
                        llDay2.setVisibility(View.GONE);
                        llDay3.setVisibility(View.VISIBLE);
                        llDay4.setVisibility(View.GONE);

                        if (alTempraturesDays3 != null && alTempraturesDays3.size() == 0) {

                            if (Utils.isNetworkAvailable(activity, false, false)) {
                                Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
                                getLocationsListDays3();
                            } else {
                                showToastMessage(getResources().getString(R.string.network_alert));
                            }
                        }

                        break;
                    case "Day4":

                        daysToAdd = 3;

                        llDay1.setVisibility(View.GONE);
                        llDay2.setVisibility(View.GONE);
                        llDay3.setVisibility(View.GONE);
                        llDay4.setVisibility(View.VISIBLE);

                        if (alTempraturesDays4 != null && alTempraturesDays4.size() == 0) {

                            if (Utils.isNetworkAvailable(activity, false, false)) {
                                Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
                                getLocationsListDays4();
                            } else {
                                showToastMessage(getResources().getString(R.string.network_alert));
                            }
                        }
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabsLayoutDaily.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getText().toString()) {
                    case "Today":

                        daysToAdd = 0;

                        if (alTempraturesDailyToday != null && alTempraturesDailyToday.size() == 0) {

                            if (Utils.isNetworkAvailable(activity, false, false)) {
                                Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
                                getLocationsListDailyToday();
                                getLocationsListDailyTomorrow();
                            } else {
                                showToastMessage(getResources().getString(R.string.network_alert));
                            }
                        }

                        llDaily1.setVisibility(View.VISIBLE);
                        llDaily2.setVisibility(View.GONE);
                        llDaily3.setVisibility(View.GONE);

                        break;
                    case "Tonight":


                        llDaily1.setVisibility(View.GONE);
                        llDaily2.setVisibility(View.VISIBLE);
                        llDaily3.setVisibility(View.GONE);

                        break;
                    case "Tomorrow":


                        llDaily1.setVisibility(View.GONE);
                        llDaily2.setVisibility(View.GONE);
                        llDaily3.setVisibility(View.VISIBLE);

                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void init() {

        tabLayoutDays.addTab(tabLayoutDays.newTab().setText(mTabsTitleDays[0]));
        tabLayoutDays.addTab(tabLayoutDays.newTab().setText(mTabsTitleDays[1]));
        tabLayoutDays.addTab(tabLayoutDays.newTab().setText(mTabsTitleDays[2]));
        tabLayoutDays.addTab(tabLayoutDays.newTab().setText(mTabsTitleDays[3]));

        tabsLayoutDaily.addTab(tabsLayoutDaily.newTab().setText(mTabsTitleDaily[0]));
        tabsLayoutDaily.addTab(tabsLayoutDaily.newTab().setText(mTabsTitleDaily[1]));
        tabsLayoutDaily.addTab(tabsLayoutDaily.newTab().setText(mTabsTitleDaily[2]));

        //[Recycler View 1 Start]
        rvLocationsDays1.setLayoutManager(new LinearLayoutManager(rvLocationsDays1.getContext()));
        rvLocationsDays1.setItemAnimator(new DefaultItemAnimator());
        rvLocationsDays1.setHasFixedSize(true);

        adapterLocationsDays1 = new LocationsAdapterDays(activity, alTempraturesDays1, new LocationsAdapterDays.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

            }
        });
        rvLocationsDays1.setAdapter(adapterLocationsDays1);
        //[Recycler View 1 End]


        //[Recycler View 2 Start]
        rvLocationsDays2.setLayoutManager(new LinearLayoutManager(rvLocationsDays2.getContext()));
        rvLocationsDays2.setItemAnimator(new DefaultItemAnimator());
        rvLocationsDays2.setHasFixedSize(true);

        adapterLocationsDays2 = new LocationsAdapterDays(activity, alTempraturesDays2, new LocationsAdapterDays.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

            }
        });
        rvLocationsDays2.setAdapter(adapterLocationsDays2);
        //[Recycler View 2 End]

        //[Recycler View 3 Start]
        rvLocationsDays3.setLayoutManager(new LinearLayoutManager(rvLocationsDays3.getContext()));
        rvLocationsDays3.setItemAnimator(new DefaultItemAnimator());
        rvLocationsDays3.setHasFixedSize(true);

        adapterLocationsDays3 = new LocationsAdapterDays(activity, alTempraturesDays3, new LocationsAdapterDays.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

            }
        });
        rvLocationsDays3.setAdapter(adapterLocationsDays3);
        //[Recycler View 3 End]

        //[Recycler View 4 Start]
        rvLocationsDays4.setLayoutManager(new LinearLayoutManager(rvLocationsDays1.getContext()));
        rvLocationsDays4.setItemAnimator(new DefaultItemAnimator());
        rvLocationsDays4.setHasFixedSize(true);

        adapterLocationsDays4 = new LocationsAdapterDays(activity, alTempraturesDays4, new LocationsAdapterDays.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

            }
        });
        rvLocationsDays4.setAdapter(adapterLocationsDays4);
        //[Recycler View 4 End]


        //[Recycler View Daily 1 Start]
        rvLocationsDailyToday.setLayoutManager(new LinearLayoutManager(rvLocationsDailyToday.getContext()));
        rvLocationsDailyToday.setItemAnimator(new DefaultItemAnimator());
        rvLocationsDailyToday.setHasFixedSize(true);

        adapterLocationsDailyToday = new LocationsAdapterDays(activity, alTempraturesDailyToday, new LocationsAdapterDays.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

            }
        });
        rvLocationsDailyToday.setAdapter(adapterLocationsDailyToday);
        //[Recycler View Daily 1 End]


        //[Recycler View Daily  2 Start]
        rvLocationsDailyTonight.setLayoutManager(new LinearLayoutManager(rvLocationsDailyTonight.getContext()));
        rvLocationsDailyTonight.setItemAnimator(new DefaultItemAnimator());
        rvLocationsDailyTonight.setHasFixedSize(true);

        adapterLocationsDailyTonight = new LocationsAdapterDays(activity, alTempraturesDailyTonight, new LocationsAdapterDays.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

            }
        });
        rvLocationsDailyTonight.setAdapter(adapterLocationsDailyTonight);
        //[Recycler View Daily  2 End]

        //[Recycler View Daily 3 Start]
        rvLocationsDailyTomorrow.setLayoutManager(new LinearLayoutManager(rvLocationsDailyTomorrow.getContext()));
        rvLocationsDailyTomorrow.setItemAnimator(new DefaultItemAnimator());
        rvLocationsDailyTomorrow.setHasFixedSize(true);

        adapterLocationsDailyTomorrow = new LocationsAdapterDays(activity, alTempraturesDailyTomorrow, new LocationsAdapterDays.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

            }
        });
        rvLocationsDailyTomorrow.setAdapter(adapterLocationsDailyTomorrow);
        //[Recycler View Daily 3 End]

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (AppCompatActivity) activity;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
        }
    }

    private void showToastMsg(String msg) {
        Toast toastMsg = Toast.makeText(activity != null ? activity : getActivity(), msg, Toast.LENGTH_SHORT);
        toastMsg.show();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
        } else {
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewHomeFragment.destroyDrawingCache();
        viewHomeFragment = null;
    }

    public void showToastMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private void getLocationsListDays1() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlTemprature;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        switch (jsonObject.getString("success")) {

                            case "1":
                                Gson gson = new Gson();
                                TempratureResponseData tempratureResponseData = gson.fromJson(response, TempratureResponseData.class);
                                alTempraturesDays1.addAll(tempratureResponseData.getData().getMain());
                                adapterLocationsDays1.notifyDataSetChanged();
                                break;

                            case "0":
                                showToastMessage(jsonObject.getString("data"));
                            default:
                                break;
                        }

                    } catch (IllegalStateException | JsonSyntaxException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("date", getDate());
                    params.put("locationId", locationId);

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } catch (IllegalStateException | JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    private void getLocationsListDays2() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlTemprature;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        switch (jsonObject.getString("success")) {

                            case "1":
                                Gson gson = new Gson();
                                TempratureResponseData tempratureResponseData = gson.fromJson(response, TempratureResponseData.class);
                                alTempraturesDays2.addAll(tempratureResponseData.getData().getMain());
                                adapterLocationsDays2.notifyDataSetChanged();
                                break;

                            case "0":
                                showToastMessage(jsonObject.getString("data"));
                            default:
                                break;
                        }

                    } catch (IllegalStateException | JsonSyntaxException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("date", getDate());
                    params.put("locationId", locationId);

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } catch (IllegalStateException | JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    private void getLocationsListDays3() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlTemprature;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        switch (jsonObject.getString("success")) {

                            case "1":
                                Gson gson = new Gson();
                                TempratureResponseData tempratureResponseData = gson.fromJson(response, TempratureResponseData.class);
                                alTempraturesDays3.addAll(tempratureResponseData.getData().getMain());
                                adapterLocationsDays3.notifyDataSetChanged();
                                break;

                            case "0":
                                showToastMessage(jsonObject.getString("data"));
                            default:
                                break;
                        }

                    } catch (IllegalStateException | JsonSyntaxException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("date", getDate());
                    params.put("locationId", locationId);

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } catch (IllegalStateException | JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    private void getLocationsListDays4() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlTemprature;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        switch (jsonObject.getString("success")) {

                            case "1":
                                Gson gson = new Gson();
                                TempratureResponseData tempratureResponseData = gson.fromJson(response, TempratureResponseData.class);
                                alTempraturesDays4.addAll(tempratureResponseData.getData().getMain());
                                adapterLocationsDays4.notifyDataSetChanged();
                                break;

                            case "0":
                                showToastMessage(jsonObject.getString("data"));
                            default:
                                break;
                        }

                    } catch (IllegalStateException | JsonSyntaxException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("date", getDate());
                    params.put("locationId", locationId);

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } catch (IllegalStateException | JsonSyntaxException e) {
            e.printStackTrace();
        }
    }


    private void getLocationsListDailyToday() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlTemprature;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        switch (jsonObject.getString("success")) {

                            case "1":
                                Gson gson = new Gson();
                                TempratureResponseData tempratureResponseData = gson.fromJson(response, TempratureResponseData.class);
                                alTempraturesDailyOne.addAll(tempratureResponseData.getData().getMain());


                                for (int i = 0; i < alTempraturesDailyOne.size(); i++) {
                                    String data = Utils.getSepareteDateToday(alTempraturesDailyOne.get(i).getHourlyWeather());

                                    if (data.equals(Constants.TODAY)) {
                                        alTempraturesDailyToday.add(alTempraturesDailyOne.get(i));
                                        adapterLocationsDailyToday.notifyDataSetChanged();

                                    } else if (data.equals(Constants.TONIGHT)) {
                                        alTempraturesDailyTonight.add(alTempraturesDailyOne.get(i));
                                        adapterLocationsDailyTonight.notifyDataSetChanged();
                                    }
                                }

                                break;

                            case "0":
                                showToastMessage(jsonObject.getString("data"));
                            default:
                                break;
                        }

                    } catch (IllegalStateException | JsonSyntaxException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("date", getDate());
                    params.put("locationId", locationId);

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } catch (IllegalStateException | JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    private void getLocationsListDailyTomorrow() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlTemprature;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        switch (jsonObject.getString("success")) {

                            case "1":
                                Gson gson = new Gson();
                                TempratureResponseData tempratureResponseData = gson.fromJson(response, TempratureResponseData.class);
                                alTempraturesDailyTwo.addAll(tempratureResponseData.getData().getMain());

                                for (int i = 0; i < alTempraturesDailyTwo.size(); i++) {
                                    String data = Utils.getSepareteDateTomorrow(alTempraturesDailyTwo.get(i).getHourlyWeather());

                                    if (data.equals(Constants.TONIGHT)) {
                                        alTempraturesDailyTonight.add(alTempraturesDailyTwo.get(i));
                                        adapterLocationsDailyTonight.notifyDataSetChanged();

                                    } else if (data.equals(Constants.TOMORROW)) {
                                        alTempraturesDailyTomorrow.add(alTempraturesDailyTwo.get(i));
                                        adapterLocationsDailyTomorrow.notifyDataSetChanged();
                                    }
                                }
                                break;

                            case "0":
                                showToastMessage(jsonObject.getString("data"));
                            default:
                                break;
                        }

                    } catch (IllegalStateException | JsonSyntaxException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("date", getDateTomorrow());
                    params.put("locationId", locationId);

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } catch (IllegalStateException | JsonSyntaxException e) {
            e.printStackTrace();
        }
    }


    private String getDate() {

        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, daysToAdd);
        String strDate = format.format(calendar.getTime());
        Log.e("strDate", "-- " + strDate);

        return strDate;
    }

    private String getDateTomorrow() {

        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        String strDate = format.format(calendar.getTime());
        Log.e("strDate", "-- " + strDate);

        return strDate;
    }

}