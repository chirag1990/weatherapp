package uk.co.chiragtailor.weatherapp.Models;

import java.io.Serializable;

/**
 * Created by Naresh on 29-05-2017.
 */

public class LocationsResponse implements Serializable {

    public String _id;
    public String name;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
