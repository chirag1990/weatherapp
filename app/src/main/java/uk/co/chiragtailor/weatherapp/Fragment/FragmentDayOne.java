package uk.co.chiragtailor.weatherapp.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import uk.co.chiragtailor.weatherapp.Adapter.LocationsAdapter;
import uk.co.chiragtailor.weatherapp.Adapter.LocationsAdapterDays;
import uk.co.chiragtailor.weatherapp.Admin.LocationsActivity;
import uk.co.chiragtailor.weatherapp.Admin.SelectDateActivity;
import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;
import uk.co.chiragtailor.weatherapp.HelperLibrary.WeatherConfig;
import uk.co.chiragtailor.weatherapp.Models.TempratureResponse;
import uk.co.chiragtailor.weatherapp.Models.TempratureResponseData;
import uk.co.chiragtailor.weatherapp.R;

public class FragmentDayOne extends Fragment implements View.OnClickListener {

    //Android Widgets---------------------------------------
    RecyclerView rvLocationsDays1;

    //------------------------------------------------------

    //Variable----------------------------------------------
    View viewHomeFragment;
    private AppCompatActivity activity;
    public String locationId = "";
    private Calendar calendar;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    LocationsAdapterDays adapterLocationsDays;
    ArrayList<TempratureResponse.Temprature> alTempratures = new ArrayList<>();
    int daysToAdd = 0;
    //------------------------------------------------------

    public FragmentDayOne() {
    }

    public static FragmentDayOne newInstance(String locationId, int daysToAdd) {

        Bundle args = new Bundle();
        args.putString("locationId", locationId);
        args.putInt("daysToAdd", daysToAdd);

        FragmentDayOne fragment = new FragmentDayOne();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getArguments();
        if (extras != null) {
            try {
                locationId = extras.getString("locationId");
                daysToAdd = extras.getInt("daysToAdd");
            } catch (Exception e) {
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewHomeFragment = inflater.inflate(R.layout.fragment_location_day_1, container, false);

        findResources();
        setListeners();
        init();

        if (Utils.isNetworkAvailable(activity, false, false)) {
            Utils.ShowProgressDialog(activity, getResources().getString(R.string.please_wait));
            getLocationsList();
        } else {
            showToastMessage(getResources().getString(R.string.network_alert));
        }


        return viewHomeFragment;
    }

    private void findResources() {
        rvLocationsDays1 = (RecyclerView) viewHomeFragment.findViewById(R.id.rvLocationsDays1);
    }

    private void setListeners() {
    }

    private void init() {

        rvLocationsDays1.setLayoutManager(new LinearLayoutManager(rvLocationsDays1.getContext()));
        rvLocationsDays1.setItemAnimator(new DefaultItemAnimator());
        rvLocationsDays1.setHasFixedSize(true);

        adapterLocationsDays = new LocationsAdapterDays(activity, alTempratures, new LocationsAdapterDays.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

            }
        });

        rvLocationsDays1.setAdapter(adapterLocationsDays);

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (AppCompatActivity) activity;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
        }
    }

    private void showToastMsg(String msg) {
        Toast toastMsg = Toast.makeText(activity != null ? activity : getActivity(), msg, Toast.LENGTH_SHORT);
        toastMsg.show();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
        } else {
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewHomeFragment.destroyDrawingCache();
        viewHomeFragment = null;
    }

    private String getDate() {

        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, daysToAdd);
        String strDate = format.format(calendar.getTime());
        Log.e("strDate", "-- " + strDate);

        return strDate;
    }

    public void showToastMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private void getLocationsList() {

        try {
            String url = WeatherConfig.url + WeatherConfig.urlTemprature;
            StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        switch (jsonObject.getString("success")) {

                            case "1":
                                Gson gson = new Gson();
                                TempratureResponseData tempratureResponseData = gson.fromJson(response, TempratureResponseData.class);
                                alTempratures.addAll(tempratureResponseData.getData().getMain());
                                adapterLocationsDays.notifyDataSetChanged();
                                break;

                            case "0":
                                showToastMessage(jsonObject.getString("data"));
                            default:
                                break;
                        }

                    } catch (IllegalStateException | JsonSyntaxException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.hideProgressDialog();
                    showToastMessage(error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("token", "62079c94-ef3e-11e5-9ce9-5e5517507c6666fe99d2-ef3e-11e5-9ce9-5e5517507c66");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("date", getDate());
                    params.put("locationId", locationId);

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
        } catch (IllegalStateException | JsonSyntaxException e) {
            e.printStackTrace();
        }
    }
}