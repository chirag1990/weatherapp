package uk.co.chiragtailor.weatherapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import uk.co.chiragtailor.weatherapp.HelperLibrary.Utils;
import uk.co.chiragtailor.weatherapp.Models.LocationsResponse;
import uk.co.chiragtailor.weatherapp.Models.TempratureResponse;
import uk.co.chiragtailor.weatherapp.R;

public class LocationsAdapterDays extends RecyclerView.Adapter<LocationsAdapterDays.ViewHolder> {

    Context mContext;
    ArrayList<TempratureResponse.Temprature> alTempratures = new ArrayList<TempratureResponse.Temprature>();
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public LocationsAdapterDays(Context context, ArrayList<TempratureResponse.Temprature> alLocations, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.alTempratures = alLocations;
        mOnItemClickListener = onItemClickListener;
    }

    /* Provide a reference to the views for each data item
     Complex data items may need more than one view per item, and
     you provide access to all the views for a data item in a view holder*/
    public class ViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case
//        public SelectableRoundedImageView iv_notification;

        public TextView tvTime, tvSummaryWeatherType, tvTemp;
        public ImageView ivIconWeatherType;
        public LinearLayout llRow;


        public ViewHolder(final View itemView) {
            super(itemView);

            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            tvSummaryWeatherType = (TextView) itemView.findViewById(R.id.tvSummaryWeatherType);
            tvTemp = (TextView) itemView.findViewById(R.id.tvTemp);
            ivIconWeatherType = (ImageView) itemView.findViewById(R.id.ivIconWeatherType);
            llRow = (LinearLayout) itemView.findViewById(R.id.llRow);

            //For Fire event on Whole view ......
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Nimesh Hirpara", Toast.LENGTH_SHORT).show();
                }
            });*/
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_days, parent, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //holder.iv_thumb.getLayoutParams().height = (int) (0.3 * DisplayMetricsHandler.getScreenWidth());
        //holder.iv_thumb.getLayoutParams().width = (int) (0.3 * DisplayMetricsHandler.getScreenWidth());

        holder.tvSummaryWeatherType.setText(alTempratures.get(position).getPrecipitation());

        if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("clear-day"))
            holder.ivIconWeatherType.setImageResource(R.drawable.suuny_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("clear-night"))
            holder.ivIconWeatherType.setImageResource(R.drawable.night_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("rain"))
            holder.ivIconWeatherType.setImageResource(R.drawable.rain_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("snow"))
            holder.ivIconWeatherType.setImageResource(R.drawable.snow_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("sleet"))
            holder.ivIconWeatherType.setImageResource(R.drawable.sleet_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("wind"))
            holder.ivIconWeatherType.setImageResource(R.drawable.wind_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("fog"))
            holder.ivIconWeatherType.setImageResource(R.drawable.fog_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("cloudy"))
            holder.ivIconWeatherType.setImageResource(R.drawable.cloudy_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("partly-cloudy-day"))
            holder.ivIconWeatherType.setImageResource(R.drawable.partly_cloudy_small);
        else if (alTempratures.get(position).getPrecipitation().equalsIgnoreCase("partly-cloudy-night"))
            holder.ivIconWeatherType.setImageResource(R.drawable.partly_cloudy_small_night);

        String temp = Utils.formatTemperature(mContext, Double.parseDouble(alTempratures.get(position).getTemp()));
        holder.tvTemp.setText(temp);
        holder.tvTime.setText(Utils.getDateFormat(alTempratures.get(position).getHourlyWeather()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return alTempratures.size();
    }
}